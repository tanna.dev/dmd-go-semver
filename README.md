# Dependency Management Data (DMD)

More info can be found at [dmd.tanna.dev](https://dmd.tanna.dev).

## Command-line tool

DMD exists as a command-line tool to simplify working with data sources used for dependency-management-data.

### Installation

Pre-built releases can be downloaded from [the GitLab Package Registry](https://gitlab.com/tanna.dev/dependency-management-data/-/packages).

Alternatively, you can install it yourself by running:

```sh
go install dmd.tanna.dev/cmd/dmd@latest
```

### Usage

For instance, if you had used [renovate-graph](https://gitlab.com/tanna.dev/renovate-graph/) to create a directory called `renovate`, you could run:

```sh
dmd db init --db dmd.db
# notice the quoting around the argument, to avoid shell globbing
dmd import renovate --db dmd.db 'renovate/*.json'
```

This will then import the files into `dmd.db` which can then be queried using i.e.:

```
sqlite3 dmd.db
SELECT * from renovate;
```

### Datasources

- Renovate, via [renovate-graph](https://gitlab.com/tanna.dev/renovate-graph/)
- Dependabot, via [dependabot-graph](https://gitlab.com/tanna.dev/dependabot-graph/)

## License

Licensed under the Apache-2.0 license.
