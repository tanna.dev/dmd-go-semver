package cmd

import (
	"database/sql"
	"fmt"

	"dmd.tanna.dev/internal/datasources/dependabot"
	"github.com/spf13/cobra"
)

var importDependabotCmd = &cobra.Command{
	Use:   "dependabot '/path/to/*.json'",
	Short: "Import a data dump from dependabot-graph",
	Long: `Takes a data export from https://gitlab.com/tanna.dev/dependabot-graph/ and converts it to the database model.

Example usage:

{dmd} import dependabot '../out/*.json' --db out.db
{dmd} import dependabot dependabot-output.json --db out.db
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cobra.CheckErr(fmt.Errorf("Missing argument"))
		}

		db, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		parser := dependabot.NewParser()
		deps, err := parser.ParseFiles(args[0])
		cobra.CheckErr(err)

		importer := dependabot.NewImporter()
		err = importer.ImportDependencies(deps, db)
		cobra.CheckErr(err)
	},
}

func init() {
	importCmd.AddCommand(importDependabotCmd)
}
