package cmd

import "github.com/spf13/cobra"

var dbGenerateCmd = &cobra.Command{
	Use:   "generate",
	Short: "Generate new data into the database",
	Long:  "On a previously initialised and `import`'d database, generate new data that can be used in conjunction",
}

func init() {
	dbCmd.AddCommand(dbGenerateCmd)
}
