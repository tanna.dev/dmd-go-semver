package cmd

import (
	"fmt"
	"os"

	"github.com/charmbracelet/log"
	"github.com/spf13/cobra"
)

var (
	databasePath string
	debug        bool
	logger       log.Logger
)

var rootCmd = &cobra.Command{
	Use:   "dmd",
	Short: "A set of tooling to interact with dependency-management-data",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		if debug {
			logger = log.New(log.WithLevel(log.DebugLevel))
			logger.Debug("Starting application in debug mode")
		} else {
			logger = log.New()
		}
	},
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func SetVersionInfo(version, commit, date string) {
	rootCmd.Version = fmt.Sprintf("%s (Built on %s from Git SHA %s)", version, date, commit)
}

func addRequiredDbFlag(cmd *cobra.Command) {
	cmd.PersistentFlags().StringVar(&databasePath, "db", "", "the path to the input/output database")
	cmd.MarkPersistentFlagRequired("db")
}

func init() {
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "whether to enable debug logging")
}
