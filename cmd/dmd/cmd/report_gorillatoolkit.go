package cmd

import (
	"database/sql"
	"fmt"

	"dmd.tanna.dev/internal/datasources"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
)

var reportGorillaToolkit = &cobra.Command{
	Use:   "gorillaToolkit",
	Short: "Query usages of the Gorilla Toolkit, which is unmaintained",
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		results, err := datasources.QueryGorillaToolkit(sqlDB)
		cobra.CheckErr(err)

		for k, v := range results {
			fmt.Println(k)

			if len(v.Direct) == 0 && len(v.Indirect) == 0 {
				fmt.Println("No dependencies found on the Gorilla toolkit")
			} else {

				if len(v.Direct) == 0 {
					fmt.Println("No direct dependencies found on the Gorilla toolkit")
				} else {
					fmt.Println("Direct dependencies")

					tw := table.NewWriter()

					tw.AppendHeader(table.Row{
						"Package", "#",
					})

					for _, dep := range v.Direct {
						tw.AppendRow(table.Row{
							dep.Name, dep.Count,
						})
					}

					fmt.Println(tw.Render())
				}

				if len(v.Indirect) == 0 {
					fmt.Println("No indirect dependencies found on the Gorilla toolkit")
				} else {
					fmt.Println("Indirect dependencies")

					tw := table.NewWriter()

					tw.AppendHeader(table.Row{
						"Package", "#",
					})

					for _, dep := range v.Indirect {
						tw.AppendRow(table.Row{
							dep.Name, dep.Count,
						})
					}

					fmt.Println(tw.Render())
				}
			}
		}
	},
}

func init() {
	reportCmd.AddCommand(reportGorillaToolkit)
}
