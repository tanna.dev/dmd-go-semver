package cmd

import (
	"database/sql"

	"dmd.tanna.dev/internal/endoflifedate"
	"github.com/spf13/cobra"
)

var dbGenerateEolCmd = &cobra.Command{
	Use:   "eol",
	Short: "Generate data to indicate whether dependencies are End Of Life (EOL) or not",
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		err = endoflifedate.Generate(logger, sqlDB)
		cobra.CheckErr(err)
	},
}

func init() {
	dbGenerateCmd.AddCommand(dbGenerateEolCmd)
}
