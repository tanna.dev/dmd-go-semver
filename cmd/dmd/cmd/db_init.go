package cmd

import (
	"database/sql"
	"fmt"

	"dmd.tanna.dev/internal/datasources"
	"github.com/spf13/cobra"
)

var dbInitCmd = &cobra.Command{
	Use:   "init",
	Short: "Initialise the local database",
	Long:  `Initialise the local database before importing data into it.`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		err = datasources.CreateTables(sqlDB)
		cobra.CheckErr(err)

		fmt.Println("Successfully initialised", databasePath)
	},
}

func init() {
	dbCmd.AddCommand(dbInitCmd)
}
