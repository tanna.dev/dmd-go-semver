package cmd

import (
	"database/sql"

	"dmd.tanna.dev/internal/endoflifedate"
	"github.com/spf13/cobra"
)

var reportEolCmd = &cobra.Command{
	Use:   "eol",
	Short: "Report the status of packages' End Of Life information",
	Long: `Report the status of packages' End Of Life information.

This uses data generated via endoflife.date, and highlights whether dependencies are lacking active support or are actively end-of-life.

Requires running one ` + "`" + `db generate eol` + "`" + ` to seed the data.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		sqlDB, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		err = endoflifedate.Report(logger, sqlDB)
		cobra.CheckErr(err)
	},
}

func init() {
	reportCmd.AddCommand(reportEolCmd)
}
