package cmd

import (
	"database/sql"
	"fmt"

	"dmd.tanna.dev/internal/datasources/renovate"
	"github.com/spf13/cobra"
)

var importRenovateCmd = &cobra.Command{
	Use:   "renovate '/path/to/*.json'",
	Short: "Import a data dump from renovate-graph",
	Long: `Takes a data export from https://gitlab.com/tanna.dev/renovate-graph/ and converts it to the database model.

Example usage:

{dmd} import renovate '../out/*.json' --db out.db
{dmd} import renovate renovate-output.json --db out.db
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cobra.CheckErr(fmt.Errorf("Missing argument"))
		}

		db, err := sql.Open("sqlite", databasePath)
		cobra.CheckErr(err)

		parser := renovate.NewParser()
		deps, err := parser.ParseFiles(args[0])
		cobra.CheckErr(err)

		importer := renovate.NewImporter()
		err = importer.ImportDependencies(deps, db)
		cobra.CheckErr(err)
	},
}

func init() {
	importCmd.AddCommand(importRenovateCmd)
}
