package cmd

import (
	"github.com/spf13/cobra"
)

var reportCmd = &cobra.Command{
	Use:   "report",
	Short: "Perform reports on the database",
	Long: `Perform reports on the database.

May require running one of the ` + "`" + `db generate` + "`" + ` subcommands to seed the data.
	`,
}

func init() {
	rootCmd.AddCommand(reportCmd)
	addRequiredDbFlag(reportCmd)
}
