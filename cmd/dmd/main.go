package main

import (
	"dmd.tanna.dev/cmd/dmd/cmd"
	_ "modernc.org/sqlite"
)

// injected by GoReleaser https://goreleaser.com/cookbooks/using-main.version/
var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
)

func main() {
	cmd.SetVersionInfo(version, commit, date)
	cmd.Execute()
}
