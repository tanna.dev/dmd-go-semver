package endoflifedate

import (
	"database/sql"
	"fmt"

	"dmd.tanna.dev/internal/datasources/renovate"
	"dmd.tanna.dev/internal/endoflifedate/client"
	"github.com/charmbracelet/log"
)

func Generate(logger log.Logger, sqlDB *sql.DB) error {
	client, err := client.NewClientWithResponses("https://endoflife.date")
	if err != nil {
		return err
	}

	err = generateRenovate(logger, sqlDB, client)
	if err != nil {
		return err
	}

	// Note that Dependabot isn't yet supported https://gitlab.com/tanna.dev/dependency-management-data/-/issues/25

	return nil
}

func generateRenovate(logger log.Logger, sqlDB *sql.DB, client *client.ClientWithResponses) error {
	deps, err := renovate.RetrieveAll(sqlDB)
	if err != nil {
		return err
	}

	renovateEOL := NewRenovate(sqlDB, client, logger)

	err = renovateEOL.CreateTables()
	if err != nil {
		return err
	}

	errors := 0
	for _, d := range deps {
		err = renovateEOL.ProcessDependency(d)
		if err != nil {
			errors++

			logger.Warn(fmt.Sprintf("Failed to process dependency (%s@%s): %v", d.PackageName, d.Version, err))
		}
	}

	if errors > 0 {
		return fmt.Errorf("%d dependencies failed to process", errors)
	}

	return nil
}
