-- name: InsertProductCycle :exec
insert into endoflifedate_products (
  product_name,
  cycle,
  supported_until,
  eol_from,
  inserted_at
  ) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?
);

------------ Renovate

-- name: InsertRenovateEndOfLife :exec
insert into renovate_endoflife (
  organisation,
  repo,

  package_name,
  version,

  package_manager,
  package_file_path,

  product_name,
  cycle
  ) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);

-- name: RetrieveRenovateEndOfLife :many
select
organisation,
repo,

package_name,
version,
package_manager,
package_file_path,

endoflifedate_products.product_name,
endoflifedate_products.cycle,

supported_until,
eol_from
from
renovate_endoflife
natural  join
endoflifedate_products
;

------------ Dependabot

-- name: InsertDependabotEndOfLife :exec
insert into dependabot_endoflife (
  organisation,
  repo,

  package_name,
  version,

  package_manager,
  package_file_path,

  product_name,
  cycle
  ) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);

-- name: RetrieveDependabotEndOfLife :many
select
organisation,
repo,

package_name,
version,
package_manager,
package_file_path,

endoflifedate_products.product_name,
endoflifedate_products.cycle,

supported_until,
eol_from
from
dependabot_endoflife
natural join
endoflifedate_products
;
