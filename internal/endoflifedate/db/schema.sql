-- endoflifedate_products contains the underlying data model for EndOfLife.date products + cycles that can be JOINed with the `*_endoflife` tables
CREATE TABLE IF NOT EXISTS endoflifedate_products (
  -- from Product

  product_name TEXT NOT NULL,

  -- from Cycle

  cycle TEXT NOT NULL,

  supported_until TEXT,
  eol_from TEXT,

  inserted_at TEXT NOT NULL,

  UNIQUE (product_name, cycle) ON CONFLICT REPLACE,
  CHECK(product_name <> ''),
  CHECK(cycle <> '')
);

-- renovate_endoflife contains mappings for the `renovate` datasource that can be JOINed with `endoflifedate_products`
CREATE TABLE IF NOT EXISTS renovate_endoflife (
  -- from renovate
  organisation TEXT NOT NULL,
  repo TEXT NOT NULL,

  package_name TEXT NOT NULL,
  version TEXT NOT NULL,

  package_manager TEXT NOT NULL,
  package_file_path TEXT NOT NULL,

  -- from endoflifedate_products
  product_name TEXT NOT NULL,
  cycle TEXT NOT NULL,

  UNIQUE (organisation, repo, package_file_path, package_name, package_manager, version) ON CONFLICT REPLACE
);

-- dependabot_endoflife contains mappings for the `dependabot` datasource that can be JOINed with `endoflifedate_products`
CREATE TABLE IF NOT EXISTS dependabot_endoflife (
  -- from dependabot
  organisation TEXT NOT NULL,
  repo TEXT NOT NULL,

  package_name TEXT NOT NULL,
  version TEXT NOT NULL,

  package_manager TEXT NOT NULL,
  package_file_path TEXT NOT NULL,

  -- from endoflifedate_products
  product_name TEXT NOT NULL,
  cycle TEXT NOT NULL,

  UNIQUE (organisation, repo, package_file_path, package_name, package_manager, version) ON CONFLICT REPLACE
);
