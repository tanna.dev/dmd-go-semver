package endoflifedate

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"

	"dmd.tanna.dev/internal/datasources/renovate"
	"dmd.tanna.dev/internal/endoflifedate/client"
	"dmd.tanna.dev/internal/endoflifedate/db"
	"github.com/charmbracelet/log"
)

var renovateParsers = []renovateParser{
	&renovateParserGo{},
	&renovateParserNodejs{},
	&renovateParserAlpine{},
	&renovateParserRedis{},
	&renovateParserPython{},
	&renovateParserRuby{},
}

type Renovate struct {
	sqlDB   *sql.DB
	queries db.Queries
	client  *client.ClientWithResponses
	logger  log.Logger
}

func NewRenovate(sqlDB *sql.DB, client *client.ClientWithResponses, logger log.Logger) Renovate {
	return Renovate{
		sqlDB:   sqlDB,
		queries: *db.New(sqlDB),
		client:  client,
		logger:  logger,
	}
}

func (r Renovate) CreateTables() (err error) {
	_, err = r.sqlDB.Exec(db.CreateTablesQuery)
	return
}

func (r Renovate) ProcessDependency(dep renovate.Dependency) error {
	product, cycle, ok := r.RetrieveEOLStatus(dep)
	if !ok {
		r.logger.Debug(fmt.Sprintf("Could not parse dependency %s as an endoflife.date Product/Cycle, returned (%v, %v, %v)", dep.DependencyDetails(), product, cycle, ok))
		return nil
	}

	if cycle.Cycle == "" {
		r.logger.Debug(fmt.Sprintf("Could not parse dependency %s as an endoflife.date Cycle, returned (%v, %v, %v)", dep.DependencyDetails(), product, cycle, ok))
		return nil
	}

	productCycleParams := db.InsertProductCycleParams{
		ProductName:    product.Name,
		Cycle:          cycle.Cycle,
		EolFrom:        emptyOrNullString(cycle.EolFrom),
		SupportedUntil: emptyOrNullString(cycle.SupportedUntil),
		InsertedAt:     time.Now().Format(time.RFC3339),
	}

	err := r.queries.InsertProductCycle(context.Background(), productCycleParams)
	if err != nil {
		return err
	}

	renovateParams := db.InsertRenovateEndOfLifeParams{
		Organisation:    dep.Organisation,
		Repo:            dep.Repo,
		PackageName:     dep.PackageName,
		Version:         dep.Version,
		PackageManager:  dep.PackageManager,
		PackageFilePath: dep.PackageFilePath,
		ProductName:     product.Name,
		Cycle:           cycle.Cycle,
	}

	err = r.queries.InsertRenovateEndOfLife(context.Background(), renovateParams)
	if err != nil {
		return err
	}

	return nil
}

func (r Renovate) RetrieveEOLStatus(dep renovate.Dependency) (p Product, c Cycle, ok bool) {
	product, cycle, ok := r.parseProductAndCycle(dep)
	if !ok {
		return
	}

	p.Name = product

	r.logger.Debug(fmt.Sprintf("Looking up endoflife.date product information for %s for dependency %s", product, dep.DependencyDetails()))
	resp, err := r.client.GetApiProductJsonWithResponse(context.Background(), product)
	if err != nil {
		return
	}

	r.logger.Debug(fmt.Sprintf("Received an HTTP %d from endoflife.date product information for %s for dependency %s", resp.StatusCode(), product, dep.DependencyDetails()))
	if resp.JSON200 == nil {
		return
	}

	cycles := *resp.JSON200
	for _, cy := range cycles {
		if cy.Cycle != nil {
			cycleStr, err := cy.Cycle.AsCycleCycle0()
			if err == nil {
				if cycle == cycleStr {
					ok = true
					c.Cycle = cycleStr

					if cy.Eol != nil {
						eol, err := cy.Eol.AsCycleEol0()
						if err == nil {
							c.EolFrom = eol
						}
					}

					if cy.Support != nil {
						supported, err := cy.Support.AsCycleSupport0()
						if err == nil {
							c.SupportedUntil = supported.Format("2006-01-02")
						}
					}
				}
			}
		}
	}

	return
}

func (r Renovate) parseProductAndCycle(dep renovate.Dependency) (p string, c string, ok bool) {
	if dep.Version == "latest" {
		return
	}

	for _, rp := range renovateParsers {
		if rp.Handled(dep) {
			p, c, ok = rp.ParseProductAndCycle(r.logger, dep)
			if ok {
				return
			}
		}
	}

	return
}

func major(s string) string {
	return part(strings.Split(s, "."), 0)
}

func majorDotMinor(s string) string {
	return fmt.Sprintf("%s.%s",
		part(strings.Split(s, "."), 0),
		part(strings.Split(s, "."), 1),
	)
}

func trimDockerSuffix(s string) string {
	return part(strings.Split(s, "-"), 0)
}

func part(parts []string, n int) string {
	if n > len(parts) {
		return ""
	}

	return parts[n]
}

func emptyOrNullString(s string) sql.NullString {
	return sql.NullString{
		String: s,
		Valid:  s != "",
	}
}
