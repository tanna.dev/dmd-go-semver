package endoflifedate

import (
	"fmt"
	"regexp"
	"strings"

	"dmd.tanna.dev/internal/datasources/renovate"
	"github.com/charmbracelet/log"
)

type renovateParser interface {
	Handled(dep renovate.Dependency) bool
	ParseProductAndCycle(logger log.Logger, dep renovate.Dependency) (p string, c string, ok bool)
}

type renovateParserGo struct{}

func (r *renovateParserGo) Handled(dep renovate.Dependency) bool {
	return dep.PackageName == "go" || dep.PackageName == "golang"
}

func (r *renovateParserGo) ParseProductAndCycle(logger log.Logger, dep renovate.Dependency) (p string, c string, ok bool) {
	p = "golang"

	parts := strings.Split(dep.Version, ".")
	if len(parts) < 2 {
		return
	}

	cycle := fmt.Sprintf("%s.%s", parts[0], trimDockerSuffix(parts[1]))

	return p, cycle, true
}

type renovateParserNodejs struct{}

func (r *renovateParserNodejs) Handled(dep renovate.Dependency) bool {
	return (dep.PackageName == "node" && dep.Datasource != "orb") || dep.PackageManager == "nodenv"
}

func (r *renovateParserNodejs) ParseProductAndCycle(logger log.Logger, dep renovate.Dependency) (p string, c string, ok bool) {
	p = "nodejs"

	r_ := regexp.MustCompile("[v >=<~^]*")
	version := r_.ReplaceAllString(dep.Version, "")

	match, _ := regexp.MatchString("^[0-9]+\\.[0-9]+\\.[0-9]+", version)
	if match {
		cycle := major(version)

		return p, cycle, true
	}

	// i.e. 18.0
	// i.e. 18.x
	match, _ = regexp.MatchString("^[0-9]+\\.[0-9a-z]+", version)
	if match {
		cycle := major(version)

		return p, cycle, true
	}

	// i.e. 18-alpine
	match, _ = regexp.MatchString("^[0-9]+-", version)
	if match {
		cycle := major(version)

		return p, cycle, true
	}

	return
}

type renovateParserAlpine struct{}

func (r *renovateParserAlpine) Handled(dep renovate.Dependency) bool {
	return dep.PackageName == "alpine"
}

func (r *renovateParserAlpine) ParseProductAndCycle(logger log.Logger, dep renovate.Dependency) (p string, c string, ok bool) {
	p = "alpine"

	r_ := regexp.MustCompile("[v >=<~^]*")
	version := r_.ReplaceAllString(dep.Version, "")

	match, _ := regexp.MatchString("^[0-9]+$", version)
	if match {
		logger.Warn(fmt.Sprintf("Couldn't assume cycle of %s that %s corresponds to", p, dep.DependencyDetails()))
		return
	}

	// i.e. 3.14
	match, _ = regexp.MatchString("^[0-9]+\\.[0-9]+", version)
	if match {
		cycle := majorDotMinor(version)

		return p, cycle, true
	}

	return
}

type renovateParserRedis struct{}

func (r *renovateParserRedis) Handled(dep renovate.Dependency) bool {
	return dep.PackageName == "redis" && dep.Datasource == "docker"
}

func (r *renovateParserRedis) ParseProductAndCycle(logger log.Logger, dep renovate.Dependency) (p string, c string, ok bool) {
	p = "redis"

	r_ := regexp.MustCompile("[v >=<~^]*")
	version := r_.ReplaceAllString(dep.Version, "")

	match, _ := regexp.MatchString("^[0-9]+$", version)
	if match {
		logger.Warn(fmt.Sprintf("Couldn't assume cycle of %s that %s corresponds to", p, dep.DependencyDetails()))
		return
	}

	// i.e. 3.14
	match, _ = regexp.MatchString("^[0-9]+\\.[0-9]+", version)
	if match {
		cycle := majorDotMinor(version)

		return p, cycle, true
	}

	return
}

type renovateParserPython struct{}

func (r *renovateParserPython) Handled(dep renovate.Dependency) bool {
	return dep.PackageName == "python" || dep.PackageManager == "pyenv"
}

func (r *renovateParserPython) ParseProductAndCycle(logger log.Logger, dep renovate.Dependency) (p string, c string, ok bool) {
	p = "python"

	r_ := regexp.MustCompile("[v >=<~^]*")
	version := r_.ReplaceAllString(dep.Version, "")

	match, _ := regexp.MatchString("^[0-9]+$", version)
	if match {
		logger.Warn(fmt.Sprintf("Couldn't assume cycle of %s that %s corresponds to", p, dep.DependencyDetails()))
		return
	}

	match, _ = regexp.MatchString("^[0-9]+\\.[0-9]+", version)
	if match {
		cycle := majorDotMinor(version)

		return p, cycle, true
	}

	return
}

type renovateParserRuby struct{}

func (r *renovateParserRuby) Handled(dep renovate.Dependency) bool {
	return dep.PackageName == "ruby" && dep.PackageManager == "ruby-version"
}

func (r *renovateParserRuby) ParseProductAndCycle(logger log.Logger, dep renovate.Dependency) (p string, c string, ok bool) {
	p = "ruby"

	r_ := regexp.MustCompile("[v >=<~^]*")
	version := r_.ReplaceAllString(dep.Version, "")

	match, _ := regexp.MatchString("^[0-9]+$", version)
	if match {
		logger.Warn(fmt.Sprintf("Couldn't assume cycle of %s that %s corresponds to", p, dep.DependencyDetails()))
		return
	}

	match, _ = regexp.MatchString("^[0-9]+\\.[0-9]+", version)
	if match {
		cycle := majorDotMinor(version)

		return p, cycle, true
	}

	match, _ = regexp.MatchString("^ruby-[0-9]+\\.[0-9]+", version)
	if match {
		cycle := majorDotMinor(strings.ReplaceAll(version, "ruby-", ""))

		return p, cycle, true
	}

	return
}
