package endoflifedate

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"dmd.tanna.dev/internal/datasources/renovate"
	"dmd.tanna.dev/internal/domain"
	"dmd.tanna.dev/internal/endoflifedate/db"
	"github.com/charmbracelet/log"
)

const daysUntilWarning = 60

func Report(logger log.Logger, sqlDB *sql.DB) error {
	eolLogger := log.New()

	queries := db.New(sqlDB)

	err := reportRenovate(logger, eolLogger, queries)
	if err != nil {
		return err
	}

	err = reportDependabot(logger, eolLogger, queries)
	if err != nil {
		return err
	}

	return nil
}

func reportRenovate(globalLogger log.Logger, eolLogger log.Logger, queries *db.Queries) error {
	rows, err := queries.RetrieveRenovateEndOfLife(context.Background())
	if err != nil {
		return err
	}

	deps := make([]renovate.Dependency, len(rows))
	products := make([]Product, len(rows))
	cycles := make([]Cycle, len(rows))
	for i, row := range rows {
		deps[i] = renovate.Dependency{
			Dependency: domain.Dependency{
				Organisation:    row.Organisation,
				Repo:            row.Repo,
				PackageName:     row.PackageName,
				Version:         row.Version,
				PackageManager:  row.PackageManager,
				PackageFilePath: row.PackageFilePath,
			},
		}

		products[i] = Product{
			Name: row.ProductName,
		}

		cycles[i] = Cycle{
			Cycle: row.Cycle,
		}
		if row.SupportedUntil.Valid {
			cycles[i].SupportedUntil = row.SupportedUntil.String
		}
		if row.EolFrom.Valid {
			cycles[i].EolFrom = row.EolFrom.String
		}
	}

	for i := 0; i < len(deps); i++ {
		dep := deps[i]
		product := products[i]
		cycle := cycles[i]

		repoPath := fmt.Sprintf("%s/%s", dep.Organisation, dep.Repo)
		depDetails := fmt.Sprintf("%s %s", repoPath, dep.DependencyDetails())

		thisLogger := eolLogger.With("repo", repoPath, "dependency", dep.DependencyDetails(), "packageFilePath", dep.PackageFilePath)

		now := time.Now()

		if cycle.EolFrom != "" {
			eol, err := time.Parse("2006-01-02", cycle.EolFrom)
			if err != nil {
				globalLogger.Warn(fmt.Sprintf("Could not parse EolFrom date from %s: %v", depDetails, err))
				continue
			}

			days := betweenDates(now, eol)
			if cycle.IsEol() {
				thisLogger.Error(fmt.Sprintf("%s's use of %s has been End-of-Life for %d days", repoPath, product.Name, abs(days)))
			}
			if reportWarning(days) {
				thisLogger.Warn(fmt.Sprintf("%s's use of %s is approaching End-of-Life status, in %d days", repoPath, product.Name, abs(days)))
			}
		}

		if cycle.SupportedUntil != "" {
			sup, err := time.Parse("2006-01-02", cycle.SupportedUntil)
			if err != nil {
				globalLogger.Warn(fmt.Sprintf("Could not parse SupportedUntil date from %s: %v", depDetails, err))
				continue
			}

			days := betweenDates(now, sup)
			if cycle.IsUnsupported() {
				thisLogger.Error(fmt.Sprintf("%s's use of %s has been without active support for %d days", repoPath, product.Name, abs(days)))
			}
			if reportWarning(days) {
				thisLogger.Warn(fmt.Sprintf("%s's use of %s is approaching the end of active support, in %d days", repoPath, product.Name, abs(days)))
			}
		}
	}

	return nil
}

func reportDependabot(globalLogger log.Logger, eolLogger log.Logger, queries *db.Queries) error {
	return nil
}

func betweenDates(t1 time.Time, t2 time.Time) int {
	hours := t2.Sub(t1).Hours()
	return int(hours) / 24
}

func reportWarning(days int) bool {
	return days > 0 && days <= daysUntilWarning
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
