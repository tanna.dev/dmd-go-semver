package domain

import (
	"fmt"
)

type Dependency struct {
	Platform     string
	Organisation string
	Repo         string

	PackageName   string
	Version       string
	LockedVersion string

	PackageManager  string
	PackageFilePath string
}

func (d Dependency) DependencyDetails() string {
	return fmt.Sprintf("(%s@%s)", d.PackageName, d.Version)
}
