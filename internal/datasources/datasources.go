package datasources

import (
	"database/sql"

	dependabotDB "dmd.tanna.dev/internal/datasources/dependabot/db"
	"dmd.tanna.dev/internal/datasources/queries"
	"dmd.tanna.dev/internal/datasources/renovate"
	renovateDB "dmd.tanna.dev/internal/datasources/renovate/db"
)

func CreateTables(db *sql.DB) (err error) {
	_, err = db.Exec(renovateDB.CreateTablesQuery)
	if err != nil {
		return
	}
	_, err = db.Exec(dependabotDB.CreateTablesQuery)
	if err != nil {
		return
	}

	return
}

func QueryMostPopularPackageManagers(db *sql.DB) (map[string][]queries.MostPopularPackageManagersRow, error) {
	all := make(map[string][]queries.MostPopularPackageManagersRow)

	result, err := renovate.QueryMostPopularPackageManagers(db)
	if err != nil {
		return nil, err
	}
	all["Renovate"] = result

	return all, nil
}

func QueryMostPopularDockerImages(db *sql.DB) (map[string]queries.MostPopularDockerImagesResult, error) {
	all := make(map[string]queries.MostPopularDockerImagesResult)

	result, err := renovate.QueryMostPopularDockerImages(db)
	if err != nil {
		return nil, err
	}
	all["Renovate"] = result

	return all, nil
}

func QueryGorillaToolkit(db *sql.DB) (map[string]queries.GorillaToolkitResult, error) {
	all := make(map[string]queries.GorillaToolkitResult)

	result, err := renovate.QueryGorillaToolkit(db)
	if err != nil {
		return nil, err
	}
	all["Renovate"] = result

	return all, nil
}

func QueryGolangCILint(db *sql.DB) (map[string]queries.GolangCILintResult, error) {
	all := make(map[string]queries.GolangCILintResult)

	result, err := renovate.QueryGolangCILint(db)
	if err != nil {
		return nil, err
	}
	all["Renovate"] = result

	return all, nil
}
