package dependabot

import (
	"context"
	"database/sql"

	"dmd.tanna.dev/internal/datasources/dependabot/db"
)

func RetrieveAll(sqlDB *sql.DB) ([]Dependency, error) {
	queries := db.New(sqlDB)

	rows, err := queries.RetrieveAll(context.Background())
	if err != nil {

		return nil, err
	}

	deps := make([]Dependency, len(rows))

	for i, r := range rows {
		deps[i] = newDependencyFromDB(r)
	}

	return deps, nil
}
