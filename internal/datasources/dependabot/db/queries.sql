-- name: InsertPackage :exec
INSERT INTO dependabot (
  platform,
  organisation,
  repo,

  package_name,
  version,
  locked_version,

  package_manager,
  package_file_path
  ) VALUES (
  ?,
  ?,
  ?,

  ?,
  ?,
  ?,

  ?,
  ?
);

-- name: RetrieveAll :many
select * from dependabot;
