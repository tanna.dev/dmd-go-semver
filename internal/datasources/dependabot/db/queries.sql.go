// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.17.0
// source: queries.sql

package db

import (
	"context"
	"database/sql"
)

const insertPackage = `-- name: InsertPackage :exec
INSERT INTO dependabot (
  platform,
  organisation,
  repo,

  package_name,
  version,
  locked_version,

  package_manager,
  package_file_path
  ) VALUES (
  ?,
  ?,
  ?,

  ?,
  ?,
  ?,

  ?,
  ?
)
`

type InsertPackageParams struct {
	Platform        string
	Organisation    string
	Repo            string
	PackageName     string
	Version         string
	LockedVersion   sql.NullString
	PackageManager  string
	PackageFilePath string
}

func (q *Queries) InsertPackage(ctx context.Context, arg InsertPackageParams) error {
	_, err := q.db.ExecContext(ctx, insertPackage,
		arg.Platform,
		arg.Organisation,
		arg.Repo,
		arg.PackageName,
		arg.Version,
		arg.LockedVersion,
		arg.PackageManager,
		arg.PackageFilePath,
	)
	return err
}

const retrieveAll = `-- name: RetrieveAll :many
select platform, organisation, repo, package_name, version, locked_version, package_manager, package_file_path from dependabot
`

func (q *Queries) RetrieveAll(ctx context.Context) ([]Dependabot, error) {
	rows, err := q.db.QueryContext(ctx, retrieveAll)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []Dependabot
	for rows.Next() {
		var i Dependabot
		if err := rows.Scan(
			&i.Platform,
			&i.Organisation,
			&i.Repo,
			&i.PackageName,
			&i.Version,
			&i.LockedVersion,
			&i.PackageManager,
			&i.PackageFilePath,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
