package dependabot

import (
	"context"
	"database/sql"
	"encoding/json"

	"dmd.tanna.dev/internal/datasources/dependabot/db"
)

type importer struct{}

func NewImporter() importer {
	return importer{}
}

func (importer) ImportDependencies(deps []Dependency, sqlDB *sql.DB) error {
	d := db.New(sqlDB)
	for _, dep := range deps {
		arg := db.InsertPackageParams{
			Platform:        dep.Platform,
			Organisation:    dep.Organisation,
			Repo:            dep.Repo,
			PackageName:     dep.PackageName,
			Version:         dep.Version,
			PackageManager:  dep.PackageManager,
			PackageFilePath: dep.PackageFilePath,
		}

		err := d.InsertPackage(context.Background(), arg)
		if err != nil {
			return err
		}
	}
	return nil
}

func serialiseDepTypes(depTypes []string) string {
	if depTypes == nil {
		// as json.Marshal will return `null` not `[]`
		return "[]"
	}

	data, err := json.Marshal(depTypes)
	if err != nil || data == nil {
		// default to an empty array to allow for parsing more easily
		return "[]"
	}

	return string(data)
}
