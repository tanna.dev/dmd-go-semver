package dependabot

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sync"

	"dmd.tanna.dev/internal/domain"
)

type parser struct{}

func NewParser() parser {
	return parser{}
}

type dependabotGraphData struct {
	Data struct {
		Repository struct {
			DependencyGraphManifests struct {
				Edges []struct {
					Node struct {
						BlobPath     string `json:"blobPath"`
						Dependencies struct {
							Nodes []struct {
								PackageManager string `json:"packageManager"`
								PackageName    string `json:"packageName"`
								Requirements   string `json:"requirements"`
							} `json:"nodes"`
						} `json:"dependencies"`
						Filename string `json:"filename"`
					} `json:"node"`
				} `json:"edges"`
				TotalCount int `json:"totalCount"`
			} `json:"dependencyGraphManifests"`
			Name  string `json:"name"`
			Owner struct {
				Login string `json:"login"`
			} `json:"owner"`
		} `json:"repository"`
	} `json:"data"`
}

func (parser) ParseFile(filename string) ([]Dependency, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var depData dependabotGraphData
	err = json.Unmarshal(data, &depData)
	if err != nil {
		return nil, err
	}

	var deps []Dependency
	for _, v := range depData.Data.Repository.DependencyGraphManifests.Edges {
		for _, dep := range v.Node.Dependencies.Nodes {
			d := Dependency{
				Dependency: domain.Dependency{
					Platform:        "github",
					Organisation:    depData.Data.Repository.Owner.Login,
					Repo:            depData.Data.Repository.Name,
					PackageName:     dep.PackageName,
					Version:         dep.Requirements,
					PackageManager:  dep.PackageManager,
					PackageFilePath: v.Node.BlobPath,
				},
			}

			deps = append(deps, d)
		}
	}
	return deps, nil
}

func (p parser) ParseFiles(glob string) ([]Dependency, error) {
	files, err := filepath.Glob(glob)
	if err != nil {
		return nil, err
	}

	if files == nil {
		return nil, fmt.Errorf("no files could be found for glob %s", glob)
	}

	var wg sync.WaitGroup
	allDeps := make([][]Dependency, len(files))

	for i, f := range files {
		wg.Add(1)

		go func(i int, f string) {
			defer wg.Done()

			deps, err := p.ParseFile(f)
			if err != nil {
				log.Printf("Failed to parse %s: %v", f, err)
				return
			}
			allDeps[i] = deps
		}(i, f)
	}

	wg.Wait()

	return flatten(allDeps), nil
}

func flatten(s [][]Dependency) []Dependency {
	var out []Dependency
	for _, v := range s {
		out = append(out, v...)
	}
	return out
}
