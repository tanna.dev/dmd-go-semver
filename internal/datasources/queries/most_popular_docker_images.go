package queries

type MostPopularDockerImagesRow struct {
	Image string
	Count int64
}

type MostPopularDockerImagesResult struct {
	Namespaces []Count
	Images     []Count
}
