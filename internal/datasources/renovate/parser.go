package renovate

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"dmd.tanna.dev/internal/domain"
)

type parser struct{}

func NewParser() parser {
	return parser{}
}

type renovateGraphData struct {
	Organisation string `json:"organisation"`
	Repo         string `json:"repo"`
	PackageData  map[string][]struct {
		Deps []struct {
			DepName     string `json:"depName"`
			PackageName string `json:"packageName"`
			// PackageManager comes from the map key
			Datasource    string   `json:"datasource"`
			DepType       string   `json:"depType"`
			DepTypes      []string `json:"depTypes"`
			CurrentValue  string   `json:"currentValue"`
			LockedVersion string   `json:"lockedVersion"`
		} `json:"deps"`
		PackageFile string `json:"packageFile"`
	} `json:"packageData"`
	Metadata struct {
		Renovate struct {
			Platform string `json:"platform"`
		} `json:"renovate"`
	} `json:"metadata"`
}

func (parser) ParseFile(filename string) ([]Dependency, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var rgData renovateGraphData
	err = json.Unmarshal(data, &rgData)
	if err != nil {
		return nil, err
	}

	var deps []Dependency
	for packageManager, v := range rgData.PackageData {
		for _, container := range v {
			for _, dep := range container.Deps {
				versions := strings.Split(dep.CurrentValue, "\n")
				for _, version := range versions {
					d := Dependency{
						Dependency: domain.Dependency{
							Platform:     rgData.Metadata.Renovate.Platform,
							Organisation: rgData.Organisation,
							Repo:         rgData.Repo,

							// PackageName is being added below
							Version:       version,
							LockedVersion: dep.LockedVersion,

							PackageManager:  packageManager,
							PackageFilePath: container.PackageFile,
						},
						Datasource: dep.Datasource,

						// DepTypes is being added below
					}

					if dep.DepName != "" {
						d.PackageName = dep.DepName
					} else if dep.PackageName != "" {
						d.PackageName = dep.PackageName
					}

					if dep.DepTypes != nil {
						d.DepTypes = dep.DepTypes
					} else if dep.DepType != "" {
						d.DepTypes = []string{dep.DepType}
					}

					deps = append(deps, d)
				}
			}
		}
	}

	return deps, nil
}

func (p parser) ParseFiles(glob string) ([]Dependency, error) {
	files, err := filepath.Glob(glob)
	if err != nil {
		return nil, err
	}

	if files == nil {
		return nil, fmt.Errorf("no files could be found for glob %s", glob)
	}

	var wg sync.WaitGroup
	allDeps := make([][]Dependency, len(files))

	for i, f := range files {
		wg.Add(1)

		go func(i int, f string) {
			defer wg.Done()

			deps, err := p.ParseFile(f)
			if err != nil {
				log.Printf("Failed to parse %s: %v", f, err)
				return
			}
			allDeps[i] = deps
		}(i, f)
	}

	wg.Wait()

	return flatten(allDeps), nil
}

func flatten(s [][]Dependency) []Dependency {
	var out []Dependency
	for _, v := range s {
		out = append(out, v...)
	}
	return out
}
