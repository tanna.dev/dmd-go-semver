package renovate

import (
	"context"
	"database/sql"
	"sort"
	"strings"

	"dmd.tanna.dev/internal/datasources/queries"
	"dmd.tanna.dev/internal/datasources/renovate/db"
)

func RetrieveAll(sqlDB *sql.DB) ([]Dependency, error) {
	queries := db.New(sqlDB)

	rows, err := queries.RetrieveAll(context.Background())
	if err != nil {

		return nil, err
	}

	deps := make([]Dependency, len(rows))

	for i, r := range rows {
		deps[i] = newDependencyFromDB(r)
	}

	return deps, nil
}

func QueryMostPopularPackageManagers(sqlDB *sql.DB) ([]queries.MostPopularPackageManagersRow, error) {
	q := db.New(sqlDB)

	rows, err := q.QueryMostPopularPackageManagers(context.Background())
	if err != nil {
		return nil, err
	}

	results := make([]queries.MostPopularPackageManagersRow, len(rows))
	for i, row := range rows {
		results[i] = queries.MostPopularPackageManagersRow{
			PackageManager: row.PackageManager,
			Count:          row.Count,
		}
	}
	return results, nil
}

func QueryMostPopularDockerImages(sqlDB *sql.DB) (result queries.MostPopularDockerImagesResult, err error) {
	q := db.New(sqlDB)

	rows, err := q.QueryMostPopularDockerImages(context.Background())
	if err != nil {
		return
	}

	namespaces := make(map[string]int64)
	images := make(map[string]int64)

	for _, row := range rows {
		images[row.PackageName] = row.Count

		var namespace string
		parts := strings.Split(row.PackageName, "/")
		if len(parts) == 1 {
			namespace = "_"
		} else if len(parts) == 2 {
			namespace = parts[0]
		} else if strings.Contains(parts[0], ".") {
			namespace = strings.Join(parts[:len(parts)-1], "/")
		} else {
			// this ideally shouldn't get hit, but if it does, return the full package name
			namespace = row.PackageName
		}

		current := namespaces[namespace]
		namespaces[namespace] = current + row.Count
	}

	result.Namespaces = descendingSort(namespaces)
	result.Images = descendingSort(images)

	return
}

func QueryGorillaToolkit(sqlDB *sql.DB) (result queries.GorillaToolkitResult, err error) {
	q := db.New(sqlDB)

	directRows, err := q.QueryGorillaToolkitDirect(context.Background())
	if err != nil {
		return
	}

	indirectRows, err := q.QueryGorillaToolkitIndirect(context.Background())
	if err != nil {
		return
	}

	directDeps := make(map[string]int64)

	for _, row := range directRows {
		directDeps[row.PackageName] = row.Count
	}

	result.Direct = descendingSort(directDeps)

	indirectDeps := make(map[string]int64)

	for _, row := range indirectRows {
		indirectDeps[row.PackageName] = row.Count
	}

	result.Indirect = descendingSort(indirectDeps)

	return
}

func QueryGolangCILint(sqlDB *sql.DB) (result queries.GolangCILintResult, err error) {
	q := db.New(sqlDB)

	directRows, err := q.QueryGolangCILintDirect(context.Background())
	if err != nil {
		return
	}

	indirectRows, err := q.QueryGolangCILintIndirect(context.Background())
	if err != nil {
		return
	}

	result.Direct = make([]queries.Repo, len(directRows))

	for i, row := range directRows {
		result.Direct[i] = queries.Repo{
			Platform:     row.Platform,
			Organisation: row.Organisation,
			Repo:         row.Repo,
		}
	}

	result.Indirect = make([]queries.Repo, len(indirectRows))

	for i, row := range indirectRows {
		result.Indirect[i] = queries.Repo{
			Platform:     row.Platform,
			Organisation: row.Organisation,
			Repo:         row.Repo,
		}
	}

	return
}

func descendingSort(m map[string]int64) []queries.Count {
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}

	sort.SliceStable(keys, func(i, j int) bool {
		return m[keys[i]] > m[keys[j]]
	})

	result := make([]queries.Count, len(keys))
	for i, key := range keys {
		result[i] = queries.Count{
			Name:  key,
			Count: m[key],
		}
	}

	return result
}
