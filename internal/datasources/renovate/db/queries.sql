-- name: InsertPackage :exec
INSERT INTO renovate (
  platform,
  organisation,
  repo,

  package_name,
  version,
  locked_version,

  package_manager,
  package_file_path,

  datasource,
  dep_types
  ) VALUES (
  ?,
  ?,
  ?,

  ?,
  ?,
  ?,

  ?,
  ?,

  ?,
  ?
);

-- name: RetrieveAll :many
select * from renovate;

-- name: QueryMostPopularPackageManagers :many
SELECT
package_manager,
count(*)
from renovate
group by
package_manager
order by count(*) desc;

-- name: QueryMostPopularDockerImages :many
select
package_name, count(package_name)
from renovate
where datasource = 'docker'
and package_name != ''
group by package_name;

-- preferably with a `json_each` but due to https://github.com/kyleconroy/sqlc/issues/1830 we need to handle it like this
-- name: QueryGorillaToolkitDirect :many
select
count(*),
package_name
from
renovate,
json_each(renovate.dep_types) as dep_type
where
package_name like 'github.com/gorilla/%'
and datasource = 'go'
and dep_type.value = 'require'
group by package_name order by count(*) DESC;

-- preferably with a `json_each` but due to https://github.com/kyleconroy/sqlc/issues/1830 we need to handle it like this
-- name: QueryGorillaToolkitIndirect :many
select
count(*),
package_name
from
renovate,
json_each(renovate.dep_types) as dep_type
where
package_name like 'github.com/gorilla/%'
and datasource = 'go'
and dep_type.value = 'indirect'
group by package_name order by count(*) DESC;

-- preferably with a `json_each` but due to https://github.com/kyleconroy/sqlc/issues/1830 we need to handle it like this
-- name: QueryGolangCILintDirect :many
select
distinct
platform,
organisation,
repo
from
renovate,
json_each(renovate.dep_types) as dep_type
where
package_name = 'github.com/golangci/golangci-lint'
and package_manager = 'gomod'
and dep_type.value = 'require';

-- preferably with a `json_each` but due to https://github.com/kyleconroy/sqlc/issues/1830 we need to handle it like this
-- name: QueryGolangCILintIndirect :many
select
distinct
platform,
organisation,
repo
from
renovate,
json_each(renovate.dep_types) as dep_type
where
package_name = 'github.com/golangci/golangci-lint'
and package_manager = 'gomod'
and dep_type.value = 'indirect';
