CREATE TABLE IF NOT EXISTS renovate (
  platform TEXT NOT NULL,
  organisation TEXT NOT NULL,
  repo TEXT NOT NULL,

  package_name TEXT NOT NULL,
  version TEXT NOT NULL,
  locked_version TEXT,

  package_manager TEXT NOT NULL,
  package_file_path TEXT NOT NULL,

  datasource TEXT NOT NULL,
  -- dep_types is a JSON array
  dep_types TEXT,

  UNIQUE (platform, organisation, repo, package_file_path, package_name, package_manager, dep_types) ON CONFLICT REPLACE
);
