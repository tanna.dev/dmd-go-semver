# Adding a new parser for EndOfLife functionality

To add sup

## Renovate

Create a new implementation of `renovateParser` in `internal/endoflifedate/renovate_parsers.go` which parses the `renovate.Dependency` and converts it to the Product and Cycle information from https://endoflife.date/.

Then, update `internal/endoflifedate/renovate.go` to make sure that `renovateParsers` contains the new parser.

## Dependabot

[Not yet supported](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/25)
